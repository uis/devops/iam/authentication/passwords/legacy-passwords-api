from fastapi.testclient import TestClient

from legacy_password_api.main import app

client = TestClient(app)


def test_healthy():
    response = client.get("/healthy")
    assert response.status_code == 200
    assert response.text == "ok"
